package util

import (
	"bufio"
	"bytes"
	"crypto/sha256"
	log "github.com/sirupsen/logrus"
	"io"
	"io/ioutil"
	"os"
	"path"
	"sort"
	"strings"
	"time"
)

// Create a new file if it does not exist. Truncates the file if it does.
// Returns a File pointer if the file could be created, nil if anything went wrong.
func createFile(fileName string) (file *os.File) {
	file, err := os.Create(fileName)
	if err == nil {
		return file
	}

	log.WithError(err).WithTime(time.Now()).Error("Could not create file '" + fileName + "'!")
	return nil
}

// Writes the mapping between customer endpoints and backends to a file.
// Format:
//   [customer name].{aveng.net|alienvault.cloud}/api [customer name]
//   [customer name].{aveng.net|alienvault.cloud} cdn
//
// Returns 'true' if everything went alright, 'false' otherwise.
func WriteFile(names []string, domain string, fileName string) bool {
	file := createFile(fileName)
	if file == nil {
		log.WithTime(time.Now()).Error("Could not write to file '" + fileName + "'!")
		return false
	}

	defer file.Close()

	writer := bufio.NewWriterSize(
		file,
		4096*2,
	)

	// Sort names first, to make this easier to compare later.
	sort.Strings(names)
	for _, name := range names {
		apiMapping := name + "." + domain + "/api " + name + "\n"
		cdnMapping := name + "." + domain + " cdn\n"

		_, err := writer.WriteString(apiMapping + cdnMapping)
		if err != nil {
			log.WithError(err).WithTime(time.Now()).Error("Could not write mapping data for instance '" + name + "'!")
			return false
		}
	}

	err := writer.Flush()
	if err != nil {
		log.WithError(err).WithTime(time.Now()).Error("Could not flush pending data to mapping file!")
		return false
	}

	return true
}

// Reads a whole file, line by line, and stores it in memory.
// Will filter out lines not matching the 'contains' strings.
// Returns the file contents, or nil if anything went wrong.
func ReadFile(fileName string, contains ...string) []string {
	file, err := os.Open(fileName)
	if err != nil {
		log.WithError(err).WithTime(time.Now()).Error("Could not read mapping data from file '" + fileName + "'!")
		return nil
	}

	defer file.Close()

	// Read file line by line, filtering out anything not matching any of the 'contains' strings.
	scanner := bufio.NewScanner(file)
	var lines []string
	for scanner.Scan() {
		line := scanner.Text()
		if contains != nil {
			for _, pattern := range contains {
				if strings.Contains(line, pattern) {
					lines = append(lines, line)
					break
				}
			}
		} else {
			lines = append(lines, line)
		}
	}

	if err := scanner.Err(); err != nil {
		log.WithError(err).WithTime(time.Now()).Error("Could not get read file '" + fileName + "'!")
		return nil
	}

	return lines
}

// Creates a symbolic link to a file, removing any previous link if it exists.
// Returns 'true' if everything is alright, 'false' otherwise.
func LinkFile(filePath string, linkPath string) bool {
	if _, err := os.Lstat(linkPath); err == nil {
		if err := os.Remove(linkPath); err != nil {
			log.WithError(err).WithTime(time.Now()).Error("Could not unlink file '" + linkPath + "'!")
			return false
		}
	}

	err := os.Symlink(filePath, linkPath)
	if err != nil {
		log.WithError(err).WithTime(time.Now()).Error("Could not create symbolic link '" + linkPath + "' to file '" + filePath + "'!")
		return false
	}

	return true
}

// Compares the content of some files.
func CompareFiles(thisFile string, thatFile string, thoseFiles ...string) bool {
	files := []string{thisFile, thatFile}
	files = append(files, thoseFiles...)
	var previousHash []byte = nil
	var currentHash []byte = nil

	for _, file := range files {
		previousHash = currentHash

		fd, err := os.Open(file)
		if err != nil {
			log.WithError(err).WithTime(time.Now()).Error("Could not open file '" + file + "' to compare!")
		}
		defer fd.Close()

		hasher := sha256.New()
		if _, err := io.Copy(hasher, fd); err != nil {
			log.WithError(err).WithTime(time.Now()).Error("Could not creat hash of file '" + file + "' to compare!")
		}

		currentHash = hasher.Sum(nil)

		if previousHash == nil {
			continue
		}

		if !bytes.Equal(currentHash, previousHash) {
			return false
		}
	}

	return true
}

// Purge all files older than 24 hours, from a given directory.
// May call Exit() if the directory is not accessible.
func PurgeOldFiles(dirName string, date time.Time) {
	dirInfo, err := ioutil.ReadDir(dirName)
	if err != nil {
		log.WithError(err).WithTime(time.Now()).Error("Could not read directory '" + dirName + "'!")
	}

	cutOff := 24 * time.Hour
	for _, fileInfo := range dirInfo {
		diff := date.Sub(fileInfo.ModTime())
		if diff > cutOff {
			err := os.Remove(path.Join(dirName, fileInfo.Name()))
			if err != nil {
				log.WithError(err).WithTime(time.Now()).Error("Could not remove the file '" + fileInfo.Name() + "'!")
			}
		}
	}
}
