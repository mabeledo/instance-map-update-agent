package util

// Returns true if a given pointer to a string is nil or empty; false otherwise.
func IsStringEmpty(s *string) bool {
	return s == nil || len(*s) <= 0
}

// Returns true if any of the given pointers to strings are nil or empty; false otherwise.
func IsAnyStringEmpty(ss ...*string) bool {
	for _, s := range ss {
		if IsStringEmpty(s) {
			return true
		}
	}

	return false
}