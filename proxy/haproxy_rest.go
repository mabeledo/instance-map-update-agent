package proxy

import (
	"bytes"
	"encoding/json"
	log "github.com/sirupsen/logrus"
	"instance-map-update-agent/util"
	"io"
	"net/http"
	"net/url"
	"time"
)

// REST based HAproxy client

type RestClient struct {
	baseURL  *url.URL
	userName string
	password string

	HttpClient *http.Client
}

type urlParameter struct {
	key   string
	value string
}

// For responses where only the _version field is needed, and all the other fields are ignored.
type version struct {
	Version uint16 `json:"_version"`
}

type transaction struct {
	Version uint16 `json:"_version"`
	Id      string `json:"id"`
	Status  string `json:"status"`
}

type httpChk struct {
	Method  string `json:"method"`
	Uri     string `json:"uri"`
	Version string `json:"version"`
}

type backend struct {
	Name string `json:"name"`
	Mode string `json:"mode"`

	Balance struct {
		Algorithm string `json:"algorithm"`
	} `json:"balance"`

	HttpChk httpChk `json:"httpchk,omitempty"`
}

type server struct {
	Name    string `json:"name"`
	Address string `json:"address"`
	Port    uint16 `json:"port"`
	Check   string `json:"check"`
	Ssl     string `json:"ssl"`
	Verify  string `json:"verify"`
}

// Create a new proxy API client.
func NewRestClient(address string, userName string, password string) *RestClient {
	client := new(RestClient)

	baseUrl, err := url.Parse(address)
	if err != nil {
		log.WithError(err).WithTime(time.Now()).Warn("Could not create new restClient with address '" + address + "'!")
		return nil
	}

	client.baseURL = baseUrl
	client.userName = userName
	client.password = password

	client.HttpClient = http.DefaultClient

	return client
}

// Creates a new backend, and attaches a new server to it.
// The whole operation is encapsulated within a transaction.
func (c *RestClient) AddBackendWithServer(backendName string, mode string, algorithm string, serverName string, address string, port uint16) bool {
	transactionId := c.CreateTransaction()
	if transactionId == "" {
		return false
	}


	// Make sure that the transaction is closed, even if any of the intermediate operations failed.
	if c.AddBackend(backendName, mode, algorithm, transactionId) &&
		c.AddServer(serverName, address, port, transactionId) {
		return c.CommitTransaction(transactionId)
	}

	return c.DeleteTransaction(transactionId)
}

// Creates a new backend in the configuration file.
func (c *RestClient) AddBackend(name string, mode string, algorithm string, currentTransactionId ...string) bool {
	if util.IsAnyStringEmpty(&name, &mode, &algorithm) {
		log.WithTime(time.Now()).Warn("One or more parameters to AddBackend are empty!")
		return false
	}

	// Create a new backend.
	newBackend := &backend{
		Name: name,
		Mode: mode,
		Balance: struct {
			Algorithm string `json:"algorithm"`
		}{
			Algorithm: algorithm,
		},
	}
	if mode == "http" {
		newBackend.HttpChk = httpChk{
			Method:  "HEAD",
			Uri:     "/",
			Version: "HTTP/1.1",
		}
	}

	var transactionIdParameter *urlParameter
	if currentTransactionId != nil {
		transactionIdParameter = &urlParameter{
			key:   "transaction_id",
			value: currentTransactionId[0],
		}
	} else {
		transactionIdParameter = nil
	}

	request := c.build(
		"POST",
		"/v1/services/haproxy/configuration/backends",
		newBackend,
		transactionIdParameter)

	if request == nil {
		return false
	}

	response := c.do(request, nil)
	if response != nil &&
		// 201: backend added.
		// 202: backend added, but reload is required.
		(response.StatusCode == 201 || response.StatusCode == 202) {
		return true
	}

	return false
}

// Creates a new server, attached to the specified backend.
func (c *RestClient) AddServer(name string, address string, port uint16, backendName string, currentTransactionId ...string) bool {
	if util.IsAnyStringEmpty(&name, &address, &backendName) || port <= 0 {
		log.WithTime(time.Now()).Warn("One or more parameters to AddServer are invalid!")
		return false
	}

	// Add a new server to the freshly created backend.
	newServer := &server{
		Name:    name,
		Address: address,
		Port:    port,
		Check:   "enabled",
		Ssl:     "enabled",
		Verify:  "none",
	}

	var parameters []*urlParameter
	parameters = append(parameters,
		&urlParameter{
			key:   "backend",
			value: backendName,
		})

	if currentTransactionId != nil {
		parameters = append(parameters,
			&urlParameter{
				key:   "transaction_id",
				value: currentTransactionId[0],
			})
	}

	request := c.build(
		"POST",
		"/v1/services/haproxy/configuration/servers",
		newServer,
		parameters...)

	if request == nil {
		return false
	}

	response := c.do(request, nil)
	if response != nil &&
		// 201: server added.
		// 202: server added, but reload is required.
		(response.StatusCode == 201 || response.StatusCode == 202) {
		return true
	}

	return false
}

// Create a new transaction that will encapsulate any following operations.
func (c *RestClient) CreateTransaction() string {
	// Keep trying for a while until a valid config file version is passed and the transaction is created.
	for i := 0; i < 5; i++ {
		// Get the configuration file version first.
		defaultsRequest := c.build("GET", "/v1/services/haproxy/configuration/defaults", nil)

		if defaultsRequest == nil {
			return ""
		}

		var currentVersion version
		versionResponse := c.do(defaultsRequest, &currentVersion)
		if versionResponse == nil || versionResponse.StatusCode != 200 {
			return ""
		}

		// Retry until a valid transaction is
		var newTransaction transaction
		request :=
			c.build(
				"POST",
				"/v1/services/haproxy/transactions",
				nil,
				&urlParameter{
					key:   "version",
					value: "2",
				})

		if request == nil {
			return ""
		}

		response := c.do(request, &newTransaction)
		if response != nil && response.StatusCode == 201 {
			return newTransaction.Id
		}
	}

	return ""
}

// Close a transaction and commit any changes in between its creation and its closing.
func (c *RestClient) CommitTransaction(transactionId string) bool {
	if util.IsStringEmpty(&transactionId) {
		log.WithTime(time.Now()).Warn("Transaction ID cannot be empty!")
		return false
	}

	request := c.build("PUT", "v1/services/haproxy/transactions/"+transactionId, nil)
	if request != nil {
		response := c.do(request, nil)

		return response != nil &&
			// 200: transaction committed.
			// 202: transaction committed, but reload is required.
			(response.StatusCode == 200 || response.StatusCode == 202)
	}
	return false
}

// Deletes a transaction, and rollbacks any changes inside it.
func (c *RestClient) DeleteTransaction(transactionId string) bool {
	if util.IsStringEmpty(&transactionId) {
		log.WithTime(time.Now()).Warn("Transaction ID cannot be empty!")
		return false
	}

	request := c.build("DELETE", "v1/services/haproxy/transactions/"+transactionId, nil)
	if request != nil {
		response := c.do(request, nil)
		return response != nil && response.StatusCode == 204
	}
	return false
}


// Build a new request.
func (c *RestClient) build(method string, path string, body interface{}, parameters ...*urlParameter) (request *http.Request) {
	referenceUrl := &url.URL{Path: path}
	absoluteUrl := c.baseURL.ResolveReference(referenceUrl)

	var buffer io.ReadWriter
	if body != nil {
		buffer = new(bytes.Buffer)
		err := json.NewEncoder(buffer).Encode(body)
		if err != nil {
			log.WithError(err).WithTime(time.Now()).Warn("Could not encode body into the request!")
			return nil
		}
	}

	// This will return an error also if 'method' is invalid.
	request, err := http.NewRequest(method, absoluteUrl.String(), buffer)
	if err != nil {
		log.WithError(err).WithTime(time.Now()).Warn("Cannot create request!")
		return nil
	}

	// Handle authentication.
	request.SetBasicAuth(c.userName, c.password)

	// Add query parameters.
	if parameters != nil {
		query := request.URL.Query()
		for _, parameter := range parameters {
			if parameter != nil {
				query.Add(parameter.key, parameter.value)
			}
		}
		request.URL.RawQuery = query.Encode()
	}

	// Handle other headers.
	request.Header.Set("Content-Type", "application/json")
	request.Header.Set("Accept", "application/json")
	return request
}

// Send a request to the endpoint, and process the response.
func (c *RestClient) do(request *http.Request, value interface{}) (response *http.Response) {
	response, err := c.HttpClient.Do(request)
	if err != nil {
		log.WithError(err).WithTime(time.Now()).Warn("Could not reach endpoint!")
		return nil
	}

	defer response.Body.Close()
	if value != nil {
		err = json.NewDecoder(response.Body).Decode(value)
		if err != nil {
			log.WithError(err).WithTime(time.Now()).Warn("Could not parse the response!")
			return nil
		}
	}
	return response
}
