package main

import (
	"flag"
	"fmt"
	"instance-map-update-agent/cloud"
	"instance-map-update-agent/proxy"
	"instance-map-update-agent/util"
	"os"
	"path"
	"time"
)

const NoEc2ClientAvailable = -1
const CannotBuildNameToIpAddressMapping = -2
const CannotWriteMappingFile = -3
const CannotWriteProxyConfig = -4

func main() {
	// Filesystem arguments.
	pathPtr := flag.String("path", "/var/cache/haproxy/", "Path to the mapping file")
	fileNamePtr := flag.String("filename", "domain2backend", "Base name for the mapping file")
	extensionPtr := flag.String("extension", "map", "Extension of the mapping file")

	// Domain argument.
	domainPtr := flag.String("domain", "alienvault.cloud", "Domain of the current environment")

	// Proxy API arguments.
	apiAddressPtr := flag.String("apiAddress", "localhost:5555", "Proxy API address")
	apiUsernamePtr := flag.String("apiUsername", "dataplaneapi", "Proxy API username")
	apiPasswordPtr := flag.String("apiPassword", "changeme", "Proxy API password")

	flag.Parse()

	mainFilePath := path.Join(*pathPtr, *fileNamePtr+"."+*extensionPtr)
	latestFilePath := path.Join(*pathPtr, fmt.Sprintf("%s-%d.%s", *fileNamePtr, time.Now().Unix(), *extensionPtr))

	// RetrieveMapping the mappings between customer subdomains (names) and backends.
	var ec2Client cloud.Ec2Client
	if ec2Client := cloud.NewEc2Client(); ec2Client == nil {
		os.Exit(NoEc2ClientAvailable)
	}

	var nameToIpAddress *map[string]string
	if nameToIpAddress := ec2Client.RetrieveMapping(); nameToIpAddress == nil {
		os.Exit(CannotBuildNameToIpAddressMapping)
	}

	// Create a new mapping file.
	newMappingFile(nameToIpAddress, *domainPtr, latestFilePath)

	// If there is new content, compared to the previous main mapping file, link the file as the current new mapping.
	// TODO: trigger the configuration update in the proxy.
	if util.CompareFiles(latestFilePath, mainFilePath) && util.LinkFile(latestFilePath, mainFilePath) {
		updateProxyConfig(nameToIpAddress, *apiAddressPtr, *apiUsernamePtr, *apiPasswordPtr)
	}
}

func newMappingFile(nameToIpAddress *map[string]string, domain string, latestFilePath string) {
	var names []string
	for name := range *nameToIpAddress {
		names = append(names, name)
	}

	// Write current domain-to-backend mappings to a temp file.
	if ! util.WriteFile(names, domain, latestFilePath) {
		os.Exit(CannotWriteMappingFile)
	}
}

func updateProxyConfig(nameToIpAddress *map[string]string, apiAddress string, apiUsername string, apiPassword string) {
	restClient := proxy.NewRestClient(apiAddress, apiUsername, apiPassword)

	for name, ipAddress := range *nameToIpAddress {
		if ! restClient.AddBackendWithServer(name, "http", "roundrobin", name, ipAddress, 443) {
			os.Exit(CannotWriteProxyConfig)
		}
	}
}
