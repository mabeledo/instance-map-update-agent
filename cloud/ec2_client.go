package cloud

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/ec2"
	"github.com/aws/aws-sdk-go/service/ec2/ec2iface"
	log "github.com/sirupsen/logrus"
	"instance-map-update-agent/util"
	"sort"
	"time"
)

type Ec2Client struct {
	Session *session.Session
	Client  ec2iface.EC2API
}

func NewEc2Client() *Ec2Client {
	ec2Client := new(Ec2Client)
	awsSession, err := session.NewSession()
	if err != nil {
		log.WithError(err).WithTime(time.Now()).Error("Could not create a new AWS session!")
		return nil
	}

	ec2Client.Session = awsSession
	ec2Client.Client = ec2.New(awsSession)

	return ec2Client
}

// RetrieveMapping instance data and return a mapping between instance names and public ip addresses.
// May quit if any of the intermediate operations fail.
func (c *Ec2Client) RetrieveMapping() (instanceToIp *map[string]string) {
	input := &ec2.DescribeInstancesInput{
		Filters: [] *ec2.Filter{
			{
				Name: aws.String("tag:Role"),
				Values: [] *string{
					aws.String("control"),
				},
			},
		},
	}

	result := make(map[string]string)

	// Iterate over all the pages.
	for {
		page, err := c.Client.DescribeInstances(input)
		if err != nil {
			log.WithError(err).WithTime(time.Now()).Error("Could not retrieve EC2 instance descriptions!")
			return nil
		}

		for key, value := range c.createMapping(page.Reservations) {
			result[key] = value
		}

		// If the NextToken field is not empty, proceed to the next page.
		// If it is empty, exit the loop.
		if !util.IsStringEmpty(page.NextToken) {
			input.SetNextToken(*page.NextToken)
		} else {
			break
		}
	}

	return &result
}

// Return a mapping between EC2 instance names and public ip addresses.
func (c *Ec2Client) createMapping(reservations []*ec2.Reservation) map[string]string {
	instanceNameToIpMapping := make(map[string]string)
	for _, reservation := range reservations {
		for _, instance := range reservation.Instances {
			nameIndex := sort.Search(
				len(instance.Tags),
				func(i int) bool { return *instance.Tags[i].Key == "Name" })

			if !util.IsStringEmpty(instance.PublicIpAddress) &&
				nameIndex < len(instance.Tags) &&
				instance.Tags[nameIndex].Value != nil {

				instanceNameToIpMapping[*instance.Tags[nameIndex].Value] = *instance.PublicIpAddress
			}
		}
	}

	return instanceNameToIpMapping
}
