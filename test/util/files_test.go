package util

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"instance-map-update-agent/util"
	"math/rand"
	"os"
	"testing"
)

func TestWrite(t *testing.T) {
	// Try to write a file in a read only directory.
	assert.False(t, util.WriteFile([]string{}, "", "/usr/bin/hola"))

	// Try to write a file in a nonexistant location.
	assert.False(t, util.WriteFile([]string{}, "", "/hola/hola"))

	// Write a file. Use ReadFile() to compare its contents to the expected data.
	filePath := fmt.Sprintf("/tmp/%s-%d.txt", "TestWrite", rand.Int())
	assert.True(t, util.WriteFile([]string{"firstSubdomain", "secondSubdomain"}, "domain", filePath))

	lines := util.ReadFile(filePath)
	assert.Equal(t, 4, len(lines))
	assert.Equal(t, "firstSubdomain.domain/api firstSubdomain", lines[0])
	assert.Equal(t, "firstSubdomain.domain cdn", lines[1])
	assert.Equal(t, "secondSubdomain.domain/api secondSubdomain", lines[2])
	assert.Equal(t, "secondSubdomain.domain cdn", lines[3])

	assert.Nil(t, os.Remove(filePath), "File '" + filePath + "' created during testing has not been deleted!")
}

func TestRead(t *testing.T) {
	// Try to read a non existent file.
	filePath := fmt.Sprintf("/tmp/%s-%d.txt", "TestRead", rand.Int())
	assert.Nil(t, util.ReadFile(filePath))

	// Read from a file but filtering out some of the lines.
	assert.True(t, util.WriteFile([]string{"firstSubdomain", "secondSubdomain"}, "domain", filePath))

	linesWithApi := util.ReadFile(filePath, "api")
	assert.Equal(t, 2, len(linesWithApi))
	assert.Equal(t, "firstSubdomain.domain/api firstSubdomain", linesWithApi[0])
	assert.Equal(t, "secondSubdomain.domain/api secondSubdomain", linesWithApi[1])

	linesWithCdn := util.ReadFile(filePath, "cdn")
	assert.Equal(t, 2, len(linesWithCdn))
	assert.Equal(t, "firstSubdomain.domain cdn", linesWithCdn[0])
	assert.Equal(t, "secondSubdomain.domain cdn", linesWithCdn[1])

	linesWithSomethingElse := util.ReadFile(filePath, "plumbus")
	assert.Equal(t, 0, len(linesWithSomethingElse))

	assert.Nil(t, os.Remove(filePath), "File '" + filePath + "' created during testing has not been deleted!")
}

func TestLink(t *testing.T) {
	// Try to link a file with no permissions.
	assert.False(t, util.LinkFile("/bin/bash", "/bin/another_bash"))

	// File to be linked does not exist.
	assert.False(t, util.LinkFile("/bin/plumbus", "/bin/another_plumbus"))

	// Actually link a file.
	filePath := fmt.Sprintf("/tmp/%s-%d.txt", "TestRead", rand.Int())
	assert.True(t, util.WriteFile([]string{"firstSubdomain", "secondSubdomain"}, "domain", filePath))
	assert.True(t, util.LinkFile(filePath, filePath + ".link"))

	assert.Nil(t, os.Remove(filePath + ".link"), "Link '" + filePath + ".link' created during testing has not been deleted!")
	assert.Nil(t, os.Remove(filePath), "File '" + filePath + "' created during testing has not been deleted!")
}

func TestCompare(t *testing.T) {
	firstFilePath := fmt.Sprintf("/tmp/%s-%d.txt", "TestWrite", rand.Int())
	assert.True(t, util.WriteFile([]string{"firstSubdomain", "secondSubdomain"}, "domain", firstFilePath))

	secondFilePath := fmt.Sprintf("/tmp/%s-%d.txt", "TestWrite", rand.Int())
	assert.True(t, util.WriteFile([]string{"thirdSubdomain", "fourthSubdomain"}, "domain", secondFilePath))

	thirdFilePath := fmt.Sprintf("/tmp/%s-%d.txt", "TestWrite", rand.Int())
	assert.True(t, util.WriteFile([]string{"firstSubdomain", "secondSubdomain"}, "domain", thirdFilePath))

	// Compare two files that are different.
	assert.False(t, util.CompareFiles(firstFilePath, secondFilePath))

	// One of the files does not exist.
	assert.False(t, util.CompareFiles(firstFilePath, "/this/file/does/not/exist"))

	// Compare two equal files.
	assert.True(t, util.CompareFiles(firstFilePath, thirdFilePath))

	assert.Nil(t, os.Remove(firstFilePath), "File '" + firstFilePath + "' created during testing has not been deleted!")
	assert.Nil(t, os.Remove(secondFilePath), "File '" + secondFilePath + "' created during testing has not been deleted!")
	assert.Nil(t, os.Remove(thirdFilePath), "File '" + thirdFilePath + "' created during testing has not been deleted!")
}

func TestPurgeOld(t *testing.T) {

}

