package proxy

import (
	"context"
	"github.com/stretchr/testify/assert"
	"instance-map-update-agent/proxy"
	"net"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestNewRestClient(t *testing.T) {
	// Fail to parse the given URL.
	firstRestClient := proxy.NewRestClient("\n", "", "")
	assert.Nil(t, firstRestClient)

	// Any other case.
	secondRestClient := proxy.NewRestClient("www.att.com", "", "")
	assert.NotNil(t, secondRestClient)
}

func TestAddBackend(t *testing.T) {
	restClient, teardown :=
		prepareTest(
			http.HandlerFunc(
				func(responseWriter http.ResponseWriter, request *http.Request) {
					assert.Equal(t, "POST", request.Method)
					assert.Equal(t, "/v1/services/haproxy/configuration/backends", request.URL.String())

					responseWriter.WriteHeader(500)
				}))

	assert.NotNil(t, restClient)

	// Test if a backend can be added with any empty parameter.
	assert.False(t, restClient.AddBackend("", "", ""))

	// Test that it returns false when the response status code is not 201, nor 202.
	assert.False(t, restClient.AddBackend("some_name", "some_mode", "some_algorithm"))
	teardown()

	// Test that it returns true if the response status code is 201.
	restClient, teardown =
		prepareTest(
			http.HandlerFunc(
				func(responseWriter http.ResponseWriter, request *http.Request) {
					assert.Equal(t, "POST", request.Method)
					assert.Equal(t, "/v1/services/haproxy/configuration/backends", request.URL.String())

					responseWriter.WriteHeader(201)
				}))

	assert.NotNil(t, restClient)
	assert.True(t, restClient.AddBackend("some_name", "some_mode", "some_algorithm"))
	teardown()

	// Test that it returns true if the response status code is 202.
	restClient, teardown =
		prepareTest(
			http.HandlerFunc(
				func(responseWriter http.ResponseWriter, request *http.Request) {
					assert.Equal(t, "POST", request.Method)
					assert.Equal(t, "/v1/services/haproxy/configuration/backends", request.URL.String())

					responseWriter.WriteHeader(202)
				}))

	assert.NotNil(t, restClient)
	assert.True(t, restClient.AddBackend("some_name", "some_mode", "some_algorithm"))
	teardown()
}

func TestAddServer(t *testing.T) {
	restClient, teardown :=
		prepareTest(
			http.HandlerFunc(
				func(responseWriter http.ResponseWriter, request *http.Request) {
					assert.Equal(t, "POST", request.Method)
					assert.Equal(t, "/v1/services/haproxy/configuration/servers?backend=test", request.URL.String())

					responseWriter.WriteHeader(500)
				}))

	assert.NotNil(t, restClient)

	// Test if a server can be added with any empty parameter.
	assert.False(t, restClient.AddServer("", "", 1, ""))

	// Test if a server can be added with a port == 0
	assert.False(t, restClient.AddServer("", "", 0, ""))

	// Test that it returns false when the response status code is not 201, nor 202.
	assert.False(t, restClient.AddServer("some_name", "some_address", 1, "test"))
	teardown()

	// Test that it returns true if the response status code is 201.
	restClient, teardown =
		prepareTest(
			http.HandlerFunc(
				func(responseWriter http.ResponseWriter, request *http.Request) {
					assert.Equal(t, "POST", request.Method)
					assert.Equal(t, "/v1/services/haproxy/configuration/servers?backend=test", request.URL.String())

					responseWriter.WriteHeader(201)
				}))

	assert.NotNil(t, restClient)
	assert.True(t, restClient.AddServer("some_name", "some_address", 1, "test"))
	teardown()

	// Test that it returns true if the response status code is 202.
	restClient, teardown =
		prepareTest(
			http.HandlerFunc(
				func(responseWriter http.ResponseWriter, request *http.Request) {
					assert.Equal(t, "POST", request.Method)
					assert.Equal(t, "/v1/services/haproxy/configuration/servers?backend=test", request.URL.String())

					responseWriter.WriteHeader(202)
				}))

	assert.NotNil(t, restClient)
	assert.True(t, restClient.AddServer("some_name", "some_address", 1, "test"))
	teardown()
}

func TestCreateTransaction(t *testing.T) {
	// Config file version retrieval fails.
	restClient, teardown :=
		prepareTest(
			http.HandlerFunc(
				func(responseWriter http.ResponseWriter, request *http.Request) {
					if request.Method == "GET" && request.URL.String() == "/v1/services/haproxy/configuration/defaults" {
						responseWriter.WriteHeader(400)
					} else {
						responseWriter.WriteHeader(500)
					}
				}))

	assert.NotNil(t, restClient)
	assert.Equal(t, "", restClient.CreateTransaction())
	teardown()

	// Version is not a number.
	restClient, teardown =
		prepareTest(
			http.HandlerFunc(
				func(responseWriter http.ResponseWriter, request *http.Request) {
					if request.Method == "GET" && request.URL.String() == "/v1/services/haproxy/configuration/defaults" {
						_, _ = responseWriter.Write([]byte(`{"_version": "no version","data": {"error_files": [{"code": 400}]}}`))
					} else {
						responseWriter.WriteHeader(500)
					}
				}))

	assert.NotNil(t, restClient)
	assert.Equal(t, "", restClient.CreateTransaction())
	teardown()

	// Transaction id is returned properly.
	restClient, teardown =
		prepareTest(
			http.HandlerFunc(
				func(responseWriter http.ResponseWriter, request *http.Request) {
					if request.Method == "GET" && request.URL.String() == "/v1/services/haproxy/configuration/defaults" {
						_, _ = responseWriter.Write([]byte(`{"_version": 2,"data": {"error_files": [{"code": 400}]}}`))
					} else if request.Method == "POST" && request.URL.String() == "/v1/services/haproxy/transactions?version=2" {
						responseWriter.WriteHeader(201)
						_, _ = responseWriter.Write([]byte(`{"_version": 2,"id": "273e3385-2d0c-4fb1-aa27-93cbb31ff203","status": "in_progress"}`))
					} else {
						responseWriter.WriteHeader(500)
					}
				}))

	assert.NotNil(t, restClient)
	assert.Equal(t, "273e3385-2d0c-4fb1-aa27-93cbb31ff203", restClient.CreateTransaction())
	teardown()
}


// Creates a mock server that will respond to requests according to the 'handler' parameter.
// Returns a new client that interacts with such server, and the server teardown function.
func prepareTest(handler http.Handler) (*proxy.RestClient, func()) {
	server := httptest.NewServer(handler)

	restClient := proxy.NewRestClient(server.URL, "", "")
	httpClient := &http.Client{
		Transport: &http.Transport{
			DialContext: func(_ context.Context, network, _ string) (net.Conn, error) {
				return net.Dial(network, server.Listener.Addr().String())
			},
		},
	}

	restClient.HttpClient = httpClient

	return restClient, server.Close
}
