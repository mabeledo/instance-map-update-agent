package cloud

import (
	"github.com/aws/aws-sdk-go/service/ec2"
	"github.com/aws/aws-sdk-go/service/ec2/ec2iface"
	"github.com/stretchr/testify/assert"
	"instance-map-update-agent/cloud"
	"reflect"
	"testing"
)

// Pattern taken from https://github.com/aws/aws-sdk-go/tree/master/example/service/sqs/mockingClientsForTests
type mockEc2Client struct {
	ec2iface.EC2API
	FirstPage  ec2.DescribeInstancesOutput
	SecondPage ec2.DescribeInstancesOutput
}

func (m mockEc2Client) DescribeInstances(input *ec2.DescribeInstancesInput) (*ec2.DescribeInstancesOutput, error) {
	// Only need to return mocked response output
	if (input.NextToken != nil) && (*input.NextToken == "secondPage") {
		return &m.SecondPage, nil
	}
	return &m.FirstPage, nil
}

func TestRetrieveMapping(t *testing.T) {
	nameTag := "Name"
	secondPage := "secondPage"
	firstName := "firstName"
	firstPublicIpAddress := "127.0.0.1"
	secondName := "secondName"
	secondPublicIpAddress := "192.168.0.1"

	testCases := []struct {
		FirstPage  ec2.DescribeInstancesOutput
		SecondPage ec2.DescribeInstancesOutput
		Expected   map[string]string
	}{
		{ // Single page of results.
			FirstPage: ec2.DescribeInstancesOutput{
				NextToken: nil,
				Reservations: []*ec2.Reservation{
					{
						Instances:
						[]*ec2.Instance{
							{
								PublicIpAddress: &firstPublicIpAddress,
								Tags: []*ec2.Tag{
									{
										Key:   &nameTag,
										Value: &firstName,
									},
								},
							},
							{
								PublicIpAddress: &secondPublicIpAddress,
								Tags: []*ec2.Tag{
									{
										Key:   &nameTag,
										Value: &secondName,
									},
								},
							},
						},
					},
				},
			},
			Expected: map[string]string{
				firstName:  firstPublicIpAddress,
				secondName: secondPublicIpAddress,
			},
		},
		{ // Two pages of results.
			FirstPage: ec2.DescribeInstancesOutput{
				NextToken: &secondPage,
				Reservations: []*ec2.Reservation{
					{
						Instances:
						[]*ec2.Instance{
							{
								PublicIpAddress: &firstPublicIpAddress,
								Tags: []*ec2.Tag{
									{
										Key:   &nameTag,
										Value: &firstName,
									},
								},
							},
						},
					},
				},
			},
			SecondPage: ec2.DescribeInstancesOutput{
				NextToken: nil,
				Reservations: []*ec2.Reservation{
					{
						Instances:
						[]*ec2.Instance{
							{
								PublicIpAddress: &secondPublicIpAddress,
								Tags: []*ec2.Tag{
									{
										Key:   &nameTag,
										Value: &secondName,
									},
								},
							},
						},
					},
				},
			},
			Expected: map[string]string{
				firstName:  firstPublicIpAddress,
				secondName: secondPublicIpAddress,
			},
		},
		{ // No results.
			Expected: map[string]string{},
		},
	}

	for _, testCase := range testCases {
		client := cloud.Ec2Client{
			Session: nil,
			Client: &mockEc2Client{
				EC2API:     nil,
				FirstPage:  testCase.FirstPage,
				SecondPage: testCase.SecondPage,
			},
		}

		assert.True(t, reflect.DeepEqual(testCase.Expected, *client.RetrieveMapping()))
	}
}
